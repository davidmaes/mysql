# MySQL
A MySQL database library that uses the PDO driver to connect.

## Getting Started
### Prerequisites
You will need docker and docker-compose to run the tests in this library.

### Installation
Just run the included composer when you first install it.
```
./bin/composer
```
## Running the tests
```
./bin/phpunit
```
## Deployment
To add this library to your application, install it with Composer
```
composer require davidmaes/mysql

```