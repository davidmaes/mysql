<?php

namespace davidmaes\mysql;

use PDO;

class MySQL
{
    /**
     * @var PDO The actual PDO connection.
     */
    private $pdo;

    /**
     * MySQL constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    }

    /**
     * Starts a MySQL transaction.
     */
    public function beginTransaction()
    {
        $this->pdo->beginTransaction();
    }

    /**
     * Prepares a query (like a prepared statement)
     *
     * @param string $query The query string.
     * @return Query A new Query object.
     */
    public function prepare($query)
    {
        return new Query($this->pdo->prepare($query));
    }

    /**
     * Commits the queries in the running transaction.
     */
    public function commit()
    {
        $this->pdo->commit();
    }

    /**
     * Rolls back all the queries in the running transaction.
     */
    public function rollback()
    {
        $this->pdo->rollBack();
    }
}