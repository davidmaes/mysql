<?php


namespace davidmaes\mysql;

use PDO;
use PDOStatement;
use stdClass;
use UnexpectedValueException;

class Query
{
    /**
     * @var PDOStatement The prepared statement of this query.
     */
    private $statement;

    /**
     * Constructs a new Query object.
     *
     * @param PDOStatement $statement The statement to execute.
     */
    public function __construct(PDOStatement $statement)
    {
        $this->statement = $statement;
    }

    /**
     * @return PDOStatement
     */
    public function getStatement(): PDOStatement
    {
        return $this->statement;
    }

    /**
     * Binds an integer parameter to the PDOStatement.
     *
     * @param string $name The parameter name.
     * @param int $value The parameter value.
     */
    public function bindInteger(string $name, int $value)
    {
        $this->statement->bindValue($name, $value, PDO::PARAM_INT);
    }

    /**
     * Binds a string parameter to the PDOStatement.
     *
     * @param string $name The parameter name.
     * @param string $value The parameter value.
     */
    public function bindString(string $name, string $value)
    {
        $this->statement->bindValue($name, $value, PDO::PARAM_STR);
    }

    /**
     * Binds a string parameter to the PDOStatement.
     *
     * @param string $name The parameter name.
     * @param bool $value The parameter value.
     */
    public function bindBoolean(string $name, bool $value)
    {
        $this->statement->bindValue($name, $value, PDO::PARAM_BOOL);
    }

    /**
     * Executes this prepared Query object.
     *
     * @throws UnexpectedValueException If the query fails, an exception is thrown.
     */
    public function execute()
    {
        if (!$this->statement->execute()) {
            throw new UnexpectedValueException('Failed to execute the query');
        }
    }

    /**
     * Returns a single row of a select query.
     *
     * @return stdClass Object holding a single row in the result set.
     */
    public function fetchSingleRow()
    {
        return $this->statement->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Returns all the remaining rows of a select query.
     *
     * @return stdClass[] An array of stdClass objects holding the result set.
     */
    public function fetchRemainingRows()
    {
        $rows = $this->statement->fetchAll(PDO::FETCH_OBJ);

        $this->close();

        return $rows;
    }

    /**
     * Closes the query's result cursor, so it can be executed again.
     */
    public function close()
    {
        $this->statement->closeCursor();
    }
}
