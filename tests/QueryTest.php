<?php

namespace test\davidmaes\mysql;

use davidmaes\mysql\Query;
use PDO;
use PDOStatement;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use stdClass;
use UnexpectedValueException;

class QueryTest extends TestCase
{
    /**
     * @throws ReflectionException
     */
    public function test___construct_getStatement()
    {
        $statement = $this->createPDOStatementMock();
        $query = new Query($statement);
        $query->getStatement();

        $this->assertEquals($statement, $query->getStatement());
    }

    /**
     *
     * @throws ReflectionException
     */
    public function test_bindInteger_callMethod()
    {
        $name = 'myInteger';
        $value = 12;

        $statement = $this->createPDOStatementMock();
        $statement
            ->expects($this->once())
            ->method('bindValue')
            ->with($name, $value, PDO::PARAM_INT);

        $query = new Query($statement);
        $query->bindInteger($name, $value);
    }

    /**
     *
     * @throws ReflectionException
     */
    public function test_bindString_callMethod()
    {
        $name = 'myString';
        $value = 'myValue';

        $statement = $this->createPDOStatementMock();
        $statement
            ->expects($this->once())
            ->method('bindValue')
            ->with($name, $value, PDO::PARAM_STR);

        $query = new Query($statement);
        $query->bindString($name, $value);
    }

    /**
     *
     * @throws ReflectionException
     */
    public function test_bindBoolean_callMethod()
    {
        $name = 'myBoolean';
        $value = true;

        $statement = $this->createPDOStatementMock();
        $statement
            ->expects($this->once())
            ->method('bindValue')
            ->with($name, $value, PDO::PARAM_BOOL);

        $query = new Query($statement);
        $query->bindBoolean($name, $value);
    }

    /**
     *
     * @throws ReflectionException
     */
    public function test_execute_success()
    {
        $statement = $this->createPDOStatementMock();
        $statement
            ->expects($this->once())
            ->method('execute')
            ->willReturn(true);

        $query = new Query($statement);
        $query->execute();
    }

    /**
     *
     * @throws ReflectionException
     */
    public function test_execute_failure()
    {
        $statement = $this->createPDOStatementMock();
        $statement
            ->expects($this->once())
            ->method('execute')
            ->willReturn(false);

        $this->expectException(UnexpectedValueException::class);
        $this->expectExceptionMessage('Failed to execute the query');

        $query = new Query($statement);
        $query->execute();
    }

    /**
     *
     * @throws ReflectionException
     */
    public function test_fetchSingleRow_getSingleStdClassObject()
    {
        $result = new stdClass();

        $statement = $this->createPDOStatementMock();
        $statement
            ->expects($this->once())
            ->method('fetch')
            ->with(PDO::FETCH_OBJ)
            ->willReturn($result);

        $query = new Query($statement);
        $this->assertEquals($result, $query->fetchSingleRow());
    }

    /**
     *
     * @throws ReflectionException
     */
    public function test_fetchRemainingRows_getArrayOfStdClassObjects()
    {
        $results = [
            new stdClass(),
            new stdClass(),
            new stdClass()
        ];

        $statement = $this->createPDOStatementMock();
        $statement
            ->expects($this->once())
            ->method('fetchAll')
            ->with(PDO::FETCH_OBJ)
            ->willReturn($results);

        $statement
            ->expects($this->once())
            ->method('closeCursor');

        $query = new Query($statement);
        $this->assertEquals($results, $query->fetchRemainingRows());
    }

    /**
     *
     * @throws ReflectionException
     */
    public function test_close_callMethod()
    {
        $statement = $this->createPDOStatementMock();
        $statement
            ->expects($this->once())
            ->method('closeCursor');

        $query = new Query($statement);
        $query->close();
    }

    /**
     * @return PDOStatement|MockObject
     * @throws ReflectionException
     */
    private function createPDOStatementMock()
    {
        return $this->getMockBuilder(PDOStatement::class)->disableOriginalConstructor()->getMock();
    }
}
