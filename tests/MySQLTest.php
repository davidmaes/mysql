<?php

namespace test\davidmaes\mysql;

use davidmaes\mysql\MySQL;
use PDO;
use PDOStatement;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class MySQLTest extends TestCase
{
    /**
     * @throws ReflectionException
     */
    public function test___construct_setErrorMode()
    {
        $pdo = $this->createPDOMock();
        $pdo->expects($this->once())->method('setAttribute')->with(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        new MySQL($pdo);
    }

    /**
     * @throws ReflectionException
     */
    public function test_beginTransaction_callMethod()
    {
        $pdo = $this->createPDOMock();
        $pdo->expects($this->once())->method('beginTransaction');
        $mysql = new MySQL($pdo);
        $mysql->beginTransaction();
    }

    /**
     * @throws ReflectionException
     */
    public function test_prepare_callMethod()
    {
        $statement = $this->createPDOStatementMock();
        $pdo = $this->createPDOMock();

        $pdo->expects($this->once())
            ->method('prepare')
            ->willReturn($statement);

        $mysql = new MySQL($pdo);
        $mysql->prepare('SELECT * FROM myTable');
    }

    /**
     * @throws ReflectionException
     */
    public function test_prepare_returnQuery()
    {
        $statement = $this->createPDOStatementMock();
        $pdo = $this->createPDOMock();

        $pdo->expects($this->once())
            ->method('prepare')
            ->willReturn($statement);

        $mysql = new MySQL($pdo);
        $query = $mysql->prepare('SELECT * FROM myTable');
        $this->assertEquals($statement, $query->getStatement());
    }

    /**
     * @throws ReflectionException
     */
    public function test_commit_callMethod()
    {
        $pdo = $this->createPDOMock();
        $pdo->expects($this->once())->method('commit');
        $mysql = new MySQL($pdo);
        $mysql->commit();
    }

    /**
     * @throws ReflectionException
     */
    public function test_rollback_callMethod()
    {
        $pdo = $this->createPDOMock();
        $pdo->expects($this->once())->method('rollback');
        $mysql = new MySQL($pdo);
        $mysql->rollback();
    }

    /**
     * @return PDO|MockObject
     * @throws ReflectionException
     */
    private function createPDOMock()
    {
        return $this->getMockBuilder(PDO::class)->disableOriginalConstructor()->getMock();
    }

    /**
     * @return PDOStatement|MockObject
     * @throws ReflectionException
     */
    private function createPDOStatementMock()
    {
        return $this->getMockBuilder(PDOStatement::class)->disableOriginalConstructor()->getMock();
    }
}
